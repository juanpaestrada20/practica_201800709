
import React from "react";
import Checkbox from "./Checkbox";

// Codigo realizado por Juan Pablo Estrada - 201800709
const TaskList = props => {
  const { list, setList } = props;

  const onChangeStatus = e => {
    const { id, checked } = e.target;

    const updateList = list.map(item => ({
      ...item,
      done: item.id === id ? checked : item.done
    }));
    setList(updateList);
  };

  const onClickRemoveItem = e => {
    const updateList = list.filter(item => !item.done);
    console.log(updateList)
    setList(updateList);
  };

  const chk = list.map(item => (
    <Checkbox key={item.id} data={item} onChange={onChangeStatus} />
  ));
  return (
    <div className="todo-list" name='todo-list'>
      {list.length ? chk : "No tasks"}
      {list.length ? (
        <p>
          <button className="button blue" name='clean-todo' onClick={onClickRemoveItem}>
            Delete all done
          </button>
        </p>
      ) : null}
    </div>
  );
};

export default TaskList;