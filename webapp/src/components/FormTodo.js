import React, { useState } from "react";

// Codigo realizado por Juan Pablo Estrada - 201800709
const FormTodo = props => {
    const { handleAddItem } = props;
    const [description, setDescription] = useState("");
    const handleSubmit = e => {
        e.preventDefault();
        handleAddItem({
        done: false,
        id: (+new Date()).toString(),
        name: 'todo-item',
        description
        });
        setDescription("");
    };
    return (
      <form onSubmit={handleSubmit}>
        <div className="todo-list">
          <div className="file-input">
            <input
              type="text"
              className="text"
              name='nuevo-todo'
              value={description}
              onChange={e => setDescription(e.target.value)}
            />
            <button
              className="button pink"
              disabled={description ? "" : "disabled"}
              name='add-todo'
            >
              Add
            </button>
          </div>
        </div>
      </form>
    );
};

export default FormTodo;