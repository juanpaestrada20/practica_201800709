
import React, { useState } from "react";

import TaskList from "./TaskList";
import FormTodo from "./FormTodo";

// Codigo realizado por Juan Pablo Estrada - 201800709
const Container = () => {
  const [list, setList] = useState([]); 

  const handleAddItem = addItem => {
    setList([...list, addItem]); 
  };
  return (
    <div>
        <h1>Práctica - 201800709</h1>
      <FormTodo handleAddItem={handleAddItem} />
      <TaskList list={list} setList={setList} />
    </div>
  );
};

export default Container;