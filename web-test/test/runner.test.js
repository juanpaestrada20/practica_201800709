const { Builder, By, until } = require('selenium-webdriver');
const { expect } = require('chai');
const USERNAME = "juanpa200799";
const KEY = 'HjTnFE4l3aCfYYU7rrwIUIwZ069nxRh3zrm6yDClDYTORrsfwp';
const HOST = 'http://34.105.37.201:3000/';

// gridUrl: gridUrl can be found at automation dashboard
const GRID_HOST = 'hub.lambdatest.com/wd/hub';

// Setup Input capabilities
const capabilities = {
    platform: 'Windows 10',
    browserName: 'Chrome',
    version: '92.0',
    resolution: '1024x768',
    network: true,
    visual: true,
    console: true,
    video: true,
    name: 'Test', // name of the test
    build: 'NodeJS build' // name of the build
}


// URL: https://{username}:{accessKey}@hub.lambdatest.com/wd/hub
const gridUrl = 'https://' + USERNAME + ':' + KEY + '@' + GRID_HOST;
describe('Pruebas funcionales para practica de ayd - 201800709', () => {
    const driver = new Builder()
        .usingServer(gridUrl)
        .withCapabilities(capabilities)
        .build();

    it('Agregar tarea a todo list', async () => {
        await driver.get(`${HOST}`);
        await driver.wait(until.elementLocated(By.className('todo-list')));
        await driver.findElement(By.name('nuevo-todo')).sendKeys('Realizar prueba funcional');
        await driver.findElement(By.name('add-todo')).click();
        await driver.sleep(2000);
        expect(driver.findElement(By.name('todo-list'))).to.not.equal(`No tasks`);
    });

    it('Marcar todo como completado', async () => {
        await driver.get(`${HOST}`);
        await driver.wait(until.elementLocated(By.className('todo-list')));
        await driver.findElement(By.name('nuevo-todo')).sendKeys('Realizar prueba funcional');
        await driver.findElement(By.name('add-todo')).click();
        await driver.sleep(2000);
        await driver.findElement(By.name('label-todo')).click();
        await driver.sleep(2000);
        expect(await driver.findElement(By.name('todo-item')).isSelected()).to.be.true;
    });

    it('Eliminar todo completado', async () => {
        await driver.get(`${HOST}`);
        await driver.wait(until.elementLocated(By.className('todo-list')));
        await driver.findElement(By.name('nuevo-todo')).sendKeys('Realizar prueba funcional');
        await driver.findElement(By.name('add-todo')).click();
        await driver.sleep(2000);
        await driver.findElement(By.name('label-todo')).click();
        await driver.sleep(2000);
        await driver.findElement(By.name('clean-todo')).click();
        await driver.sleep(2000);
        expect(await driver.findElement(By.name('todo-list')).getText()).to.be.equal('No tasks');
        await driver.close();
    });
})
